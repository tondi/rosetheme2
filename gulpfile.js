var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var livereload = require('gulp-livereload');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var autoprefixer = require('gulp-autoprefixer');

var plumberErrorHandler = {
    errorHandler: notify.onError({
        title: 'Gulp',
        message: 'Error: <%= error.message %>'

    })

};

gulp.task('sass', function() {
    gulp.src('./css/src/style.scss')
        .pipe(plumber(plumberErrorHandler))
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('.'))
        .pipe(livereload());

    gulp.src('noscript.scss')
        .pipe(plumber(plumberErrorHandler))
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('.'))
        .pipe(livereload());
});

// doesnt work
// gulp.task('php', function () {
//   gulp.src('./*.php')
//     .pipe(livereload());
// });


// gulp.task('js', function () {
//     gulp.src('js/src/*.js')
//     .pipe(concat('theme.js'))
//     .pipe(gulp.dest('js'));
// });

gulp.task('watch', function() {
    livereload.listen();
    gulp.watch('css/src/**/*.scss', ['sass']); // nasluchuje zarowno w dir jak i /*
    //gulp.watch('./*.php', ['php']); 
    gulp.watch('./noscript.scss', ['sass']);
    // gulp.watch('js/src/*.js', ['js']);
    gulp.watch('img/src/*.{png,jpg,gif}', ['img']);
});

gulp.task('default', ['sass', 'watch']);