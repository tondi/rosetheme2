<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package rosetheme2
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Wygląda na to, że nie ma strony o takim adresie.', 'rosetheme2' ); ?></h1>
				</header><!-- .page-header -->

				<a class="frontpage-href" href="/centrumroza">Przejdź do strony głównej</a>
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer('404'); ?>
