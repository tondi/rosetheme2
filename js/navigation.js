/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
(function() {

    var siteNav = document.getElementById("site-navigation");
    var content = document.getElementsByClassName("entry-content")[0];
    // transparent at top by default
    // siteNav.classList.add("transparentBg");
    var styleElem = document.head.appendChild(document.createElement("style")); // external style adds on scroll
    // var metaTheme = document.head.querySelector("meta[name=theme-color]").content;
    var flag = true;


    // TESTING Open menu by slide
    
    /*
    var start;
    var actual;
    var nav = document.querySelector(".main-navigation");

    function clicked(e) {
        
        // console.log(e)
        actual = e.touches[0].clientY;
        var pos = 1.8 * (actual - start);
        pos = pos.toString();

        // console.log(typeof pos, pos)
        nav.style.transform = "translateY(" + pos + "px)";
        nav.style.transition = "0.1s";
        // console.log(nav.style)
		
	}

    window.addEventListener("touchstart", function (e) {
        start = e.touches[0].clientY;
        // console.log(start)
        window.addEventListener("touchmove", clicked)
    })
    window.addEventListener("touchend", function () {
        window.removeEventListener("touchmove", clicked)
        nav.style.transform = "translateY(0px)";
        
    })
    */


    window.addEventListener("scroll", function() {
        // For browser compatibility reasons
        // http://stackoverflow.com/questions/28633221/document-body-scrolltop-firefox-returns-0-only-js
        var scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0; 
        
        scrollTop > 150 ? siteNav.classList.remove("transparentBg") : siteNav.classList.add("transparentBg");
        if (scrollTop > 500) {

            // console.log(window.innerWidth / window.innerHeight) 
            // console.log(scrollTop)

            if (flag) {
                // console.log("dzialam")
                styleElem.innerHTML = `
                    @media screen and (min-width: 1281px), (min-height: 688px), (max-width: 992px){
                        #main .entry-header::before{
                            z-index: -3;
                        }
                    }

                    body:not(.home) #main .entry-header::before {
                        z-index: -3;
                    }

                `;
                flag = false;
                // document.getElementById("site-navigation").innerHTML = "active"

                
            }
        } else {
            flag = true;
            styleElem.innerHTML = `
                    @media screen and (min-width: 1281px), (min-height: 688px), (max-width: 992px){
                        #main .entry-header::before {
                            z-index: -2;
                        }
                    }

                    body:not(.home) #main .entry-header::before {
                        z-index: -2;
                    }                    

            `;
            // document.getElementById("site-navigation").innerHTML = "inactive"

        }

        // var mainBg = document.querySelectorAll(".entry-header::before");
        // // console.log(document.styleSheets[1])
        // document.body.scrollTop > 250 ? document.styleSheets[1].addRule('.entry-header::before { height: 600px }', 0) : console.log("xx");
        // document.styleSheets[0].addRule('.entry-header::before', 'color: green');


        // #main {
        //     // z-index: 1;
        //     .content-colored {
        //         color: $rosebak;
        //     }
        //     .entry-header {
        //         // position: relative;
        //         z-index: 1;
        //         &::before {
        //             position: fixed;

    })

    var container, button, menu, links, i, len;
    container = document.getElementById('site-navigation');
    if (!container) {
        return;
    }

    button = document.getElementById('hamburger');
    // console.log(button)
    if ('undefined' === typeof button) {
        return;
    }

    menu = container.getElementsByTagName('ul')[0];

    // Hide menu toggle button if menu is empty and return early.
    if ('undefined' === typeof menu) {
        button.style.display = 'none';
        return;
    }

    menu.setAttribute('aria-expanded', 'false');
    if (-1 === menu.className.indexOf('nav-menu')) {
        menu.className += ' nav-menu';
    }

    // 	let hamburger = document.getElementById("hamburger");
    // let flag = false;
    // hamburger.addEventListener("click", () => {
    //     // console.log(hamburger)
    //     if(!flag){
    //         hamburger.classList.toggle("collapsed");
    //     } else {
    //         hamburger.classList.toggle("collapsed");
    //     }

    // })
    metaTheme = "#280821";

    button.onclick = function() {
        // console.log("click")
        if (-1 !== container.className.indexOf('toggled')) {
            container.className = container.className.replace(' toggled', '');
            button.setAttribute('aria-expanded', 'false');
            button.classList.toggle("collapsed");
            menu.setAttribute('aria-expanded', 'false');
        } else {
            container.className += ' toggled';
            button.classList.toggle("collapsed");
            button.setAttribute('aria-expanded', 'true');
            menu.setAttribute('aria-expanded', 'true');
        }
    };

    // Get all the link elements within the menu.
    links = menu.getElementsByTagName('a');

    // Each time a menu link is focused or blurred, toggle focus.
    for (i = 0, len = links.length; i < len; i++) {
        links[i].addEventListener('focus', toggleFocus, true);
        links[i].addEventListener('blur', toggleFocus, true);
    }

    /**
     * Sets or removes .focus class on an element.
     */
    function toggleFocus() {
        var self = this;

        // Move up through the ancestors of the current link until we hit .nav-menu.
        while (-1 === self.className.indexOf('nav-menu')) {

            // On li elements toggle the class .focus.
            if ('li' === self.tagName.toLowerCase()) {
                if (-1 !== self.className.indexOf('focus')) {
                    self.className = self.className.replace(' focus', '');
                } else {
                    self.className += ' focus';
                }
            }

            self = self.parentElement;
        }
    }

    /**
     * Toggles `focus` class to allow submenu access on tablets.
     */
    (function(container) {
        var touchStartFn, i,
            parentLink = container.querySelectorAll('.menu-item-has-children > a, .page_item_has_children > a');

        if ('ontouchstart' in window) {
            touchStartFn = function(e) {
                var menuItem = this.parentNode,
                    i;

                if (!menuItem.classList.contains('focus')) {
                    e.preventDefault();
                    for (i = 0; i < menuItem.parentNode.children.length; ++i) {
                        if (menuItem === menuItem.parentNode.children[i]) {
                            continue;
                        }
                        menuItem.parentNode.children[i].classList.remove('focus');
                    }
                    menuItem.classList.add('focus');
                } else {
                    menuItem.classList.remove('focus');
                }
            };

            for (i = 0; i < parentLink.length; ++i) {
                parentLink[i].addEventListener('touchstart', touchStartFn, false);
            }
        }
    }(container));
})();